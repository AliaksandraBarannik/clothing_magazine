package services;


import org.testng.Assert;
import pages.ContactUsPage;

public class ContactUsService {
    private ContactUsPage contactUsPage = new ContactUsPage();

    public void checkHeader() {
        Assert.assertEquals(contactUsPage.getHeaderText(), contactUsPage.expectedHeader(), "Verify header");
    }
}
