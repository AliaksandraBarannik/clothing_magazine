package services;

import pages.BasePage;

public class BaseService {
    private BasePage basePage = new BasePage();

    public LoginService openLoginPage(){
        basePage.clickLoginButton();
        return new LoginService();
    }

    public ContactUsService openContactUsPage(){
        basePage.clickContactUsButton();
        return new ContactUsService();
    }
}
