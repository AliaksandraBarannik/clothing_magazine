package services;

import org.testng.Assert;
import pages.LoginPage;

public class LoginService {
    private LoginPage loginPage = new LoginPage();

    public void checkHeader(){
        Assert.assertEquals(loginPage.getHeaderText(), loginPage.getExpectedHeader(), "Verify header");
    }
}
