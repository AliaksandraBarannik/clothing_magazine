package pages;

import org.openqa.selenium.By;

public class ContactUsPage extends BasePage {

    private static String HEADER = "CUSTOMER SERVICE - CONTACT US";

    private By header = By.xpath("//h1[@class='page-heading bottom-indent']");

    public String getHeaderText(){
        return driver.findElement(header).getText();
    }

    public String expectedHeader(){
        return HEADER;
    }
}
