package pages;

import driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BasePage {

    enum MenuSections{
        WOMEN("Women"),
        DRESSES("Dresses"),
        T_SHIRTS("T-shirts");

        private String section;

        MenuSections(String section){
            this.section = section;
        }

        public String getSection(){
            return section;
        }

        private static List<String> sections = new ArrayList<>();
        public static List<String> getSections() {
            if (sections.isEmpty()) {
                sections = Arrays.asList(MenuSections.values()).stream()
                        .map(MenuSections::getSection)
                        .collect(Collectors.toList());
            }
            return sections;
        }
    }

    protected WebDriver driver;

    private By logo = By.id("header_logo");
    private By searchInput = By.id("search_query_top");
    private By submitSearchButton = By.name("submit_search");
    private By cart = By.cssSelector("div.shopping_cart a");
    private By menu = By.xpath("//ul[contains(@class, 'sf-menu')]");
    private By contactUsButton = By.cssSelector("div#contact-link a");
    private By loginButton = By.cssSelector("a.login");

    public BasePage() {
        this.driver = Driver.getWebDriverInstance();
    }

    public LoginPage clickLoginButton() {
        driver.findElement(loginButton).click();
        return new LoginPage();
    }

    public SearchPage searchThing(String searchValue){
        driver.findElement(searchInput).sendKeys(searchValue);
        driver.findElement(submitSearchButton).click();
        return new SearchPage();
    }

    public ContactUsPage clickContactUsButton() {
        driver.findElement(contactUsButton).click();
        return new ContactUsPage();
    }

    public List<String> getExpectedSectionNameList(){
        return MenuSections.getSections();
    }
}
