package pages;

import org.openqa.selenium.By;

public class LoginPage extends BasePage {
    private static String HEADER = "AUTHENTICATION";

    private By header = By.cssSelector(".page-heading");

    public String getHeaderText(){
        return driver.findElement(header).getText();
    }

    public String getExpectedHeader(){
        return HEADER;
    }
}
