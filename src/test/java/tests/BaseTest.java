package tests;

import driver.Driver;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.*;
import services.BaseService;
import services.ContactUsService;
import services.LoginService;

import java.util.Map;

public class BaseTest {

    private static  String URL = "http://automationpractice.com/index.php";
    private static WebDriver driver;
//    private Map<String, String> parameters;

    protected BaseService service;
    private LoginService loginService;
    private ContactUsService contactUsService;

//    @BeforeSuite
//    public void setParameters(ITestContext context){
//        parameters = context.getCurrentXmlTest().getAllParameters();
//    }

    @BeforeMethod
    public void setUp() {
        driver = Driver.getWebDriverInstance();
//        driver.get(parameters.get(URL));
        driver.get(URL);
     //   driver.get(System.getProperty("url")!=null ? System.getProperty("url") : URL);
        service = new BaseService();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test(groups = {"regression"})
    public void test1() {
        contactUsService = service.openContactUsPage();
        contactUsService.checkHeader();
    }

    @Test(groups = {"smoke"})
    public void test2() {
        loginService = service.openLoginPage();
        loginService.checkHeader();
    }
}
